# README #

This is a repository for the documents for cloud computing and app development at Northwest Tech.  

### What is this repository for? ###

* We are using this to keep a versioning system of our competencies and syllabi.

### Install ###
Clone into a directory
cd into the main-documents folder
npm install  

### Tools ###

#### npm run data ####
* This rounds up all the json files and displays them in one page based on semester and course.
NOTE - to exit, press control + c in the console to stop the server. 

#### npm run new ####
* creates a new course json file from a template.

#### npm run help ####
* Shows a basic web page with help tutorials on how to use the software

### Who do I talk to? ###

* Jeremy Skrdlant
* Andrew Strange
