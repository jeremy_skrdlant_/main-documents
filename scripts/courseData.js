let fs = require('fs');

function getCourseData(semesterNumber){
  let coursesData = {};
  let allCourses = fs.readdirSync(`Semester ${semesterNumber}`);

  allCourses.forEach((courseFile, index) => {
    let courseData = JSON.parse(fs.readFileSync(`Semester ${semesterNumber}/${courseFile}`));
    let courseName = courseFile.split(".")[0];
    coursesData[courseName] = courseData;
  });
  return coursesData;
}

module.exports = {getCourseData};
