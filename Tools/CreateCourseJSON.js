const prompt = require('prompt-sync')();
var fs = require('fs');

//Get File Path Info
let fileName = prompt("What is the course name? ")
let semesterNumber = prompt("What semester number is this course in? ")
let fullPath = `../Semester ${semesterNumber}/${fileName}.json`;

//Read in the template
let templateData = JSON.parse(fs.readFileSync('courseTemplate.json'));

//Get Competencies and add to template
let roughList = prompt("Paste Competencies");
templateData.competencies = roughList.replace(/\t/g, "").split("\n").map(item => item.trim());

//Write out to the file
fs.writeFileSync(fullPath, JSON.stringify(templateData, null, 2));
