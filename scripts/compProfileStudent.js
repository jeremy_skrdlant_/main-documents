let hasStudents = false;

document.getElementById("addStudent").addEventListener("click", ()=>{
  let studentName = prompt("Enter a Student");
  let studentList = document.querySelector("#currentStudents");
  if (studentName != null){
    if (!hasStudents){
      document.querySelector(".studentName").innerHTML = studentName;
      studentList.classList.remove("hidden");
      hasStudents = true;
      studentList.innerHTML = studentName;
    }else{
      var page = document.querySelector(".page:last-child");
      var clone = page.cloneNode(true);
      clone.querySelector(".studentName").innerHTML = studentName;
      page.after(clone);
      studentList.innerHTML += ", " + studentName;
    }
  }

})
