let express = require('express');
let handlebars = require('express-handlebars');
let fs = require('fs');
let cmd = require('node-cmd');
let app = express();
let {getCourseData} = require('./scripts/courseData.js');
const args = process.argv.slice(2);

//Setup handlebars and Static Routes
app.engine('handlebars',
    handlebars({defaultLayout: 'main',
                      helpers:{"inc": (value)=>{return parseInt(value) + 1}}
              }));
app.set('view engine', 'handlebars');
app.use(express.static('styles'));
app.use(express.static('scripts'));

//Shows all Courses and base data.
app.get("/", (req, resp)=>{
  let everyCourse = [];
  for (var semesterNumber = 1; semesterNumber <= 4; semesterNumber++)
  {
    everyCourse.push(getCourseData(semesterNumber));
  }
  resp.render('allClasses', {semesters:everyCourse})
});

app.get("/handbook", (req, resp)=>{
  let everyCourse = [];
  for (var semesterNumber = 1; semesterNumber <= 4; semesterNumber++)
  {
    everyCourse.push(getCourseData(semesterNumber));
  }
  resp.render('handbook', {semesters:everyCourse})
});

//Shows Help info about the project.
app.get("/help", (req, resp)=>{
  resp.render('help');
});

//This builds an individual Syllabus.
app.get("/generateSyllabus/:semesterNumber/:courseName/:instructor", (req, resp)=>{
  let semesterNumber = req.params.semesterNumber;
  let courseName = req.params.courseName;
  let instructor = req.params.instructor;
  let courseData = JSON.parse(fs.readFileSync(`Semester ${semesterNumber}/${courseName}.json`));
  let programData = JSON.parse(fs.readFileSync("Program/details.json"));
  let chosenInstructorData = programData.instructors[instructor];
  if (semesterNumber == "1" || semesterNumber == "2"){
    courseData.placement = "Freshman";
  }else{
    courseData.placement = "Sophomore";
  }
  resp.render('syllabus', {layout:"syllabusLayout.handlebars", semester:semesterNumber, data:courseData, name:courseName, program:programData, instructor:chosenInstructorData});
});

//This builds an individual Comp Profile.
app.get("/generateCompProfile/:semesterNumber", (req, resp)=>{
  let semesterNumber = req.params.semesterNumber;
  let courses = getCourseData(semesterNumber);
  let programData = JSON.parse(fs.readFileSync("Program/details.json"));
  //calculate total hours from credit hours and hours.
  let totalHours = 0;
   for (course in courses){
      totalHours += courses[course].hours * courses[course].creditHours;
   }
   let info = {totalHours: totalHours, year: programData.currentYear}
   switch(semesterNumber){
     case "1":
      info["semesterLabel"] = "1st Year - 1st Semester";
      info["startDate"] = programData.fallStartDate;
      info["endDate"] = programData.fallEndDate;
      break;
     case "2":
      info["semesterLabel"] = "1st Year - 2nd Semester";
      info["startDate"] = programData.springStartDate;
      info["endDate"] = programData.springEndDate;
      break;
     case "3":
       info["semesterLabel"] = "2nd Year - 1st Semester";
       info["startDate"] = programData.fallStartDate;
       info["endDate"] = programData.fallEndDate;
       break;
      case "4":
        info["semesterLabel"] = "2nd Year - 2nd Semester";
        info["startDate"] = programData.springStartDate;
        info["endDate"] = programData.springEndDate;
        break;
   }
  resp.render('compProfile', {layout:"compLayout.handlebars", courses:courses, other:info});
});

const port = process.env.PORT || 3000;
app.listen(port, ()=>{
  console.log(`We are running on port ${port}.\n Press Control + C to quit.`);
  //open up a browser to the default spot.
  if (args[0] == "help"){
    cmd.run(`open http://localhost:${port}/help`);
  }else{
    cmd.run(`open http://localhost:${port}`);
  }
});
